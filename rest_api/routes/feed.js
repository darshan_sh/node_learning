const express = require("express");

const router = express.Router();

const { body } = require('express-validator/check');

const feedsController = require('../controllers/feed');

router.get('/posts', feedsController.getPosts);

router.post('/post', [
    body('title').trim().isLength({ min: 5 }),
    body('content').trim().isLength({ min: 5 }),
], feedsController.postPosts);

router.get('/post/:postId', feedsController.getPost);

router.put('post/:postId', [
    body('title').trim().isLength({ min: 5 }),
    body('content').trim().isLength({ min: 5 }),
], feedsController.updatePost);

router.delete('post/:postId', feedsController.deletePost);

module.exports = router;