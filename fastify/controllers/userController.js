const User = require('../userModel');

exports.createUser = async (request, reply) => {
    const checkuser = await User.findOne({ email: request.body.email });
    if (checkuser) {
        throw new Error("Email already exists");
    }
    try {
        const firstName = request.body.firstName;
        const lastName = request.body.lastName;
        const email = request.body.email;

        const user = new User({ firstName, lastName, email });
        await user.save()
        reply.status(200).send({ user: "user created" });
    }
    catch (err) {
        throw new Error(err);
    }
}

exports.readUser = async (request, reply) => {
    if (request.params.userId) {
        try {
            let userId = request.params.userId;
            const user = await User.findById(userId)
            if (!user) {
                throw new Error("User not found");
            }
            reply.status(200).send({ user: user });
        }
        catch (err) {
            throw new Error(err);
        }
    }
    else {
        const page = request.query.page;
        let NO_ITEMS_PER_PAGE = 0;
        if (page) {
            NO_ITEMS_PER_PAGE = 2;
        }
        try {
            const users = await User.find()
                .skip((page - 1) * NO_ITEMS_PER_PAGE)
                .limit(NO_ITEMS_PER_PAGE);
            if (!users) {
                throw new Error("User not found");
            }
            reply.status(200).send({ users: users });
        }
        catch (err) {
            throw new Error(err);
        }
    }
}

exports.updateUser = async (request, reply) => {
    const userId = request.params.userId;
    const firstName = request.body.firstName;
    const lastName = request.body.lastName;
    const email = request.body.email;
    try {
        const user = await User.findById(userId);
        if (!user) {
            throw new Error("User not found");
        }
        user.firstName = firstName;
        user.lastName = lastName;
        user.email = email;
        await user.save();
        reply.status(200).send({ message: "User updated" });
    }
    catch (err) {
        throw new Error(err);
    }

}

exports.deleteUser = async (request, reply) => {
    try {
        const userId = request.params.userId;
        const user = await User.findByIdAndDelete(userId);
        if (!user) {
            throw new Error("User not found");
        }
        reply.status(200).send({ message: "User deleted", user: user });
    }
    catch (err) {
        throw new Error(err);
    }

}