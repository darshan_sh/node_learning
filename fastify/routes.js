const userController = require('./controllers/userController');

const User = require('./userModel');

const opts = {
    schema: {
        body: {
            type: 'object',
            properties: {
                firstName: { type: 'string' },
                lastName: { type: 'string' },
                email: { type: 'string' }
            }
        }
    },
    onRequest: function (request, reply, done) {
        console.log("in onrequest");
        done()
    },
    onResponse: function (request, reply, done) {
        console.log("in onresponse");
        done()
    },
    preParsing: function (request, reply, done) {
        console.log("in preparsing");
        done()
    },
    preValidation: function (request, reply, done) {
        console.log("in prevalidation");
        done()
    },
    preHandler: function (request, reply, done) {
        console.log("in prehandler");
        done()
    },
    onSend: (request, reply, payload, done) => {
        console.log("in onsend");
        done(null, payload)
    },


}

const routes = async (fastify, options) => {
    fastify.post('/create', opts, userController.createUser);
    fastify.get('/read/:userId', userController.readUser);
    fastify.get('/read/', userController.readUser);
    fastify.put('/update/:userId', opts, userController.updateUser);
    fastify.delete('/delete/:userId', userController.deleteUser);
};

module.exports = routes;