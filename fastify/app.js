const fastify = require('fastify')({
    logger: true,
    ignoreTrailingSlash: true
});

require('dotenv-safe').config();

const mongoose = require('mongoose');

const MONGO_URI = 'mongodb+srv://' + process.env.MONGO_USER + ':' + process.env.MONGO_PASSWORD + '@cluster0.gr3be.mongodb.net/' + process.env.MONGO_DATABASE + '?retryWrites=true&w=majority';

fastify.register(require('./routes'), { prefix: '/user' });
console.log(process.env.PORT);
mongoose.connect(MONGO_URI)
    .then(() => {
        fastify.listen(process.env.PORT, (err, address) => {
            if (err) {
                fastify.log.error(err)
                process.exit(1)
            }
            fastify.log.info(`server listening on ${address}`);

        })
    })
    .catch(err => {
        console.log(err);
    })