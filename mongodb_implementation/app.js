//main file
const express = require("express");

const mongoose = require('mongoose');

const path = require("path");

const bodyParser = require("body-parser");

const multer = require("multer");

const fileStorage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'images');
    },
    filename: (req, file, cb) => {
        cb(null, file.originalname);
    }
});

const fileFilter = (req, file, cb) => {
    if (file.mimetype == 'image/jpeg' || file.mimetype == 'image/jpg' || file.mimetype == 'image/png') {
        cb(null, true);

    } else {
        cb(null, false);
    }
}

const session = require('express-session')

const MongoStore = require('connect-mongodb-session')(session);

const rootDir = require("./path");
const adminRoutes = require("./admin");
const shopRoutes = require("./shop");

const app = express();
const MONGO_URI = 'mongodb+srv://shdarshan:4Yi1xPGBHBlXMDLF@cluster0.gr3be.mongodb.net/shop?retryWrites=true&w=majority';

const store = new MongoStore({
    uri: MONGO_URI,
    collection: 'sessions',

});

app.use(bodyParser.urlencoded({ extended: false }));
app.use(multer({ storage: fileStorage, fileFilter: fileFilter }).single('image'));
app.use(session({
    secret: 'secretkey',
    resave: false,
    saveUninitialized: false,
    store: store
}));

app.use(shopRoutes);
app.use('/admin', adminRoutes);

app.use((req, res, next) => {
    res.status(404).sendFile(path.join(rootDir, 'views', '404.html'));
})

mongoose.connect(MONGO_URI)
    .then(() => {
        app.listen(80);
    })
    .catch(err => {
        console.log("not connected", err);
    });
