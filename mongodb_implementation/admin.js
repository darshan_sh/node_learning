const express = require('express');

const Product = require('./schemas').Product;

const User = require('./schemas').User;

const bcrypt = require('bcryptjs');

const { check } = require('express-validator/check');

const { validationResult } = require('express-validator/check');


const path = require('path');

const rootDir = require('./path');

const router = express.Router();

router.get('/add-product', (req, res, next) => {
  // Product.findById(/*give id here or pass params*/)
  //   .then(products => {
  //     console.log(products);
  res.sendFile(path.join(rootDir, 'views', 'add-product.html'));
  // })
  // .catch(err => {
  // console.log(err);
  // })
});

router.post('/add-product', (req, res, next) => {
  const title = req.body.title;
  const price = req.body.price;
  const quantity = req.body.quantity;
  const image = req.file;
  if (!image) {
    console.log(image);
    res.redirect('/add-product')
  }
  const product = new Product({
    title: title,
    price: price,
    quantity: quantity,
    imageUrl: image.path
  });
  product
    .save()
    .then(() => {
      res.redirect('/');
    })
    .catch(err => {
      console.log(err);
    });
});

router.get('/signup', (req, res, next) => {
  res.sendFile(path.join(rootDir, 'views', 'signup.html'));
})

router.post('/signup', [
  check('email').isEmail().withMessage("Provide valid email").custom((value, { req }) => {
    return User.findOne({ email: value })
      .then(userInfo => {
        if (userInfo) {
          return Promise.reject("Already exists, pick another");
        }
      })
  }),
  check('password', "Minimum length of password should be at least 5 characters").isLength({ min: 5 }),
  check('confirmPassword', "Passwords must match").custom((value, { req }) => {
    if (value !== req.body.password) {
      throw new Error();
    }
    return true;
  })
],
  (req, res, next) => {
    // console.log(req.body);
    const email = req.body.email;
    const password = req.body.password;
    const errors = validationResult(req);

    if (errors["errors"].length > 0) {
      console.log(errors);
      return res.status(422).redirect('/admin/signup');
    }

    bcrypt.hash(password, 15)
      .then(hashedPassword => {
        const user = new User({
          email: email,
          password: hashedPassword
        });
        return user.save();
      })
      .then(result => {
        // console.log("user added");
        res.redirect('/')
      })
      .catch((err) => {
        console.log(err);
      });
  })



router.get('/login', (req, res, next) => {
  res.sendFile(path.join(rootDir, 'views', 'signin.html'));
});

router.post('/login', (req, res, next) => {
  const email = req.body.email;
  const password = req.body.password;
  const errors = validationResult(req);
  if (errors["errors"].length > 0) {
    console.log(errors);
    return res.status(422).redirect('/admin/login');
  }
  User.findOne({ email: email })
    .then(userInfo => {
      if (!userInfo) {
        return res.redirect("/admin/login");
      }
      bcrypt.compare(password, userInfo.password)
        .then(matching => {
          if (matching) {
            req.session.isLoggedIn = true;
            return res.redirect('/');
          }
          return res.redirect('/admin/login');
        })
        .catch(err => {
          console.log(err);
        });
    })
})


router.post('/logout', (req, res, next) => {
  console.log("Logged out");
  req.session.destroy(() => {
    res.redirect('/')
  })
})
/****************deletion using an ID*****************/
// router.get('/delete', (req, res, next) => {
//   Product.findByIdAndRemove("6027b5d14580e830e018b4f6")
//     .then(() => {
//       console.log("Deleted");
//       res.redirect('/');
//     })
//     .catch(err => {
//       console.log(err);
//     })
// })



module.exports = router;