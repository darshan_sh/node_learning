/****************filter, map, reduce *******************/
var arr = [10, 20, 30, 40, 50];
var person = {
  name: "DARSHAN",
  age: 21,
};
const arr1 = [];

console.log(arr.filter((item) => item < 25)); //filter function
arr.map((item) => {
  arr1.push(item); //map function to copy an array
});
const doubled = arr1.map((item) => item * 2);

const sum = arr.reduce((s, item) => s + item); //reduce function to find sum
const sum1 = arr.reduce((s, item) => s + item, 10); //reduce function to find sum with initial value 10

console.log(arr, arr1, doubled, sum, sum1);

/****************Bitwise operators*******************/
var and = 6 & 2;
var or = 6 | 2;
var xor = 6 ^ 2;
var not = ~6;
var left_shift = 6 << 2; //zero-filled left-shift
var right_shift = 6 >> 2; //signed right-shift
var right_shift1 = 6 >>> 2; //zero-filled right-shift
console.log(and, or, xor, not, left_shift, right_shift, right_shift1);

/****************logical operators*******************/
var item;
for (item of arr) {
  if (item <= 45 && item >= 25) {
    //logical AND
    console.log(item);
  }
  if (item == 20 || item <= 40) {
    //logical OR
    console.log(item);
  }
  if (!item) {
    //logical NOT
    console.log(item);
  }
}

/****************ternary operator*******************/
console.log(2 < 5 ? "true" : "false");
var name = "Darshan";
var sentence = `My name is ${name}.`;
console.log(sentence);

//arrow functions used in all the examples

/****************spread / rest operator*******************/
const arr_spread = [...arr]; //...used as spread
console.log(arr_spread);
function display_as_array(...args) {
  //used as rest
  return args;
}
console.log(display_as_array(1, 2));

/****************destructuring******************/
const [a, b, ...rest] = arr;
console.log(a, b, rest);

const { name: n = "abc", age: ag } = person; //default value and renaming
console.log(n, ag);

/****************conditionally add object properties******************/
const movie = {
  name: "movie name",
  ...(1 > 2 /*some condition*/ && { rating: 7 }),
};
console.log(movie);

//padStart
var str = "hello";
console.log(str.padStart(4, "$"));
console.log(str.padEnd(4, "$"));
