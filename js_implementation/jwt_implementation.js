const jwt = require("jsonwebtoken");

const payload = {
  name: "darshan",
  age: 21,
};
const privateKey = "astalavista1234";

const token = jwt.sign(payload, privateKey, { expiresIn: "10s" });
console.log(token);

setTimeout(() => {
  const data = jwt.verify(token, privateKey);
  console.log(data);
}, 4000);
