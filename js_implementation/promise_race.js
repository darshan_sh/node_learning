var p1 = Promise.resolve(100);
var p2 = Promise.resolve(200);

Promise.race([p1, p2]).then((value) => {
    console.log(value);
});


/****************logging the promises for its state - empty array will always stay pending************/

var foreverPendingPromise = Promise.race([]);
console.log(foreverPendingPromise);
setTimeout(function () {
    console.log('the stack is now empty');
    console.log(foreverPendingPromise);
});


/****************logging the promises for its state************/

var foreverPendingPromise = Promise.race([]);
var alreadyFulfilledProm = Promise.resolve(100);

var arr = [foreverPendingPromise, alreadyFulfilledProm, "non-Promise value"];
var arr2 = [foreverPendingPromise, "non-Promise value", Promise.resolve(100)];
var p = Promise.race(arr);
var p2 = Promise.race(arr2);

console.log(p);
console.log(p2);
setTimeout(function () {
    console.log(p);
    console.log(p2);
});