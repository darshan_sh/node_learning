/****************funcitons as arguments****************/
// const isEven = (num) => num % 2 === 0;
// const arr = [1, 2, 3, 4, 5];
// result = arr.filter(isEven);
// console.log(result);

/***********************returning a function*********************/
const add = (x) => (y) => x + y;

const add10 = add(10);
const result = add10(20);
console.log(add10);
console.log(result);