const momHappy = false; //change to true for resolved output

const getPhone = new Promise((resolve, reject) => {
  if (momHappy) {
    const phone = {
      brand: "Samsung",
      color: "black",
    };
    resolve(phone);
  } else {
    const reason = new Error("Mom not happy");
    reject(reason);
  }
});

const showFriend = (phone) => {
  const message = `I've got a new ${phone.brand} of ${phone.color} color.`;
  return Promise.resolve(message);
};

const askmom = () => {
  getPhone
    .then(showFriend)
    .then((message) => console.log(message))
    .catch((error) => console.log(error.message))
    .finally(() => console.log("Finally called irrespective of output"));
};

askmom();
