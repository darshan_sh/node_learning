var p1 = Promise.resolve(100);
var p2 = Promise.resolve(200);
var p3 = Promise.resolve(300);
Promise.allSettled([p1, p2, p3]).then((values) => {// returns array of promise objects
    values.forEach((value) => {
        console.log(value.value);
    })
})

/****************for empty array it returns back an empty array with first priority****************/
Promise.allSettled([]).then((values) => {
    console.log(values);

})