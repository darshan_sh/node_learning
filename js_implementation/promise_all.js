const p1 = Promise.resolve(3);
const p3 = 545;
const p2 = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve("hello");
  }, 10000);
});

Promise.all([p1, p2, p3]).then((values) => {
  console.log(values);
});


/****************logging the promises for its state************/

var resolvedPromisesArray = [Promise.resolve(33), Promise.resolve(44)];

var p = Promise.all(resolvedPromisesArray);

console.log(p);

setTimeout(function () {
  console.log("the stack is now empty");
  console.log(p);
});


/****************rejected promise************/

var p1 = new Promise((resolve, reject) => {
  resolve("One");
});
var p2 = new Promise((resolve, reject) => {
  setTimeout(() => resolve("two"), 2000);
});
var p3 = new Promise((resolve, reject) => {
  setTimeout(() => reject(new Error("three")), 3000);
});
var p4 = new Promise((resolve, reject) => {
  setTimeout(() => resolve("four"), 4000);
});
Promise.all([p1, p2, p3, p4])
  .then((values) => {
    console.log(values);
  })
  .catch((message) => {
    console.error(message.message);
  });


/****************handling rejections************/

var p1 = new Promise((resolve, reject) => {
  resolve("One");
});
var p2 = new Promise((resolve, reject) => {
  setTimeout(() => resolve("two"), 2000);
});
var p3 = new Promise((resolve, reject) => {
  setTimeout(() => reject(new Error("three")), 3000);
});
var p4 = new Promise((resolve, reject) => {
  setTimeout(() => resolve("four"), 4000);
});
Promise.all([
  p1.catch((error) => {
    return error;
  }),
  p2.catch((error) => {
    return error;
  }),
  p3.catch((error) => {
    return error;
  }),
  p4.catch((error) => {
    return error;
  }),
]).then((values) => {
  console.log(values);
});
