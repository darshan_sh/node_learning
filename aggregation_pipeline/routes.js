const express = require('express');

const router = express.Router();

const User = require('./model');

router.get('/read', (req, res, next) => {
    User.aggregate([
        // { $match: { firstname: 'nihar' } },
        // { $group: { _id: '$firstname', count: { $sum: 1 }, name: { $push: "$lastname" } } }
        { $group: { _id: '$firstname', names: { $push: "$$ROOT" } } }

    ])
        .then(user => {
            res.status(200).json({ user: user });
        })
        .catch(err => {
            if (!err.statusCode) {
                err.statusCode = 500;
            }
            next(err);
        })
});

module.exports = router;