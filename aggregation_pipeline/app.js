const express = require('express');

const mongoose = require('mongoose');

const routes = require('./routes');

const app = express();

const MONGO_URI = 'mongodb+srv://shdarshan:4Yi1xPGBHBlXMDLF@cluster0.gr3be.mongodb.net/users?retryWrites=true&w=majority';

app.use('/getdata', routes);

app.use((error, req, res, next) => {
    console.log(error);
    const status = error.statusCode || 500;
    const message = error.message;
    res.status(status).json({ message: message });
})

mongoose.connect(MONGO_URI)
    .then(result => {
        console.log("connected");
        app.listen(80);
    })
    .catch(err => {
        console.log(err);
    })