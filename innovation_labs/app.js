/*********************Realease 15/02/2021***************************/

/************************* ES7 ************************/
// var a = 2;
// var b = 3;
// var expo = a ** b; //exponentiatioN operator introduced in ES7
// console.log(expo);

// var arr = [1, 2, 3, 4, 5, 6, 7, 8, 9];
// console.log(arr.includes(2, 3));




/*************************** ES8 ************************/
// var str = "hello";
// console.log(str.padStart(10, "$")); //ES8
// console.log(str.padEnd(10, "$"));

// var a = [1, 2, 3, , , ,]; //ES8
// console.log(a);
// console.log(a.length); //outputs 6
// console.log(a[4]); //outputs undefined

/****Object entries ****/
// var obj = { 100: 'a', 2: 'b', 7: 'c' };
// var arr = Object.entries(obj); //ES8
// var objB = Object.fromEntries(arr); //ES10
// var arrvalues = Object.values(obj) //ES8
// console.log(obj);
// console.log(arr);
// console.log(objB);
// console.log(arrvalues);





/**************** ES9 for await of and promise.finally *****************/
// function* generator() {
//     try {
//         yield 0;
//         yield 1;
//         yield Promise.resolve(2);
//         yield Promise.reject(3);
//         yield 4;
//         throw 5;
//     } finally {
//         console.log('called finally')
//     }
// }


// (async function () {
//     try {
//         for await (let num of generator()) { //ES9
//             console.log(num);
//         }
//     }
//     catch (e) { //catch argument can be omitted in ES10
//         console.log('caught', e);
//     }
// })();







/************************** ES10 *********************************/
// var a = [1, 7, [2, 3], [4, [5, [6]]]];
// console.log(a.flat(3)); //ES10 
// console.log(a.flatMap(item => [[item * 2]])); //ES10

/******** Trimming***********/
// var str = "    hello    ";
// console.log(str.length);
// console.log(str.trim());
// console.log(str.trim().length);
// var str = "    hello    ";
// console.log(str.length);
// console.log(str.trimStart());
// console.log(str.trimStart().length);
// var str = "    hello    ";
// console.log(str.length);
// console.log(str.trimEnd());
// console.log(str.trimEnd().length);


/*********************Release 27/02/2021**************************/

//ES2020
//Optional chaining
// let color = null;
// const user = {
//     name: 'dsh',
//     age: 21,
//     address: {
//         street: {
//             number: 52,
//             block: 4
//         },
//         city: 'Bangalore'
//     }
// };

// console.log(color[1]);
// console.log(color?.[1]);

// //Dynamic impporting
// document.getElementById('someId')
//     .addEventListener('click', async () => {
//         const { myFunc } = await import('someFile');
//         myFunc();
//     })

//nullish coalescing operator
// console.log(null ?? 'dsh');
// console.log(null ?? false);
// console.log(undefined ?? 'dsh');
// console.log(0 ?? 'dsh');
// console.log(NaN ?? 'dsh');

//matchAll()
// const regex = /[a-o]/g;
// const str = 'darshan';
// const iterator = str.matchAll(regex);
// console.log(iterator);
// Array.from(iterator, (result) => {
//     console.log(result);
// });

//replaceAll() works only on NODE 15
// let str = 'I ate puri, the puri was oily, so puri is not good for health';
// str = str.replaceAll('puri', 'chapati');
// console.log(str);

//numeric seperator
// const n = 100000000;
// console.log(n);

//ListFormat()
// const arr = ['Pen', 'Pencil', 'Paper']
// const obj = new Intl.ListFormat('en', { style: 'short', type: 'conjunction' })
// const obj = new Intl.ListFormat('it', { style: 'short', type: 'disjunction' });
// console.log(obj.format(arr))

//DateTimeFormat
// let date = new Intl.DateTimeFormat('en', { timeStyle: 'long' })
// let date = new Intl.DateTimeFormat('en', { dateStyle: 'long' })
// let date = new Intl.DateTimeFormat('it', { timeStyle: 'long', dateStyle: 'short' })

// console.log(date.format(Date.now()))