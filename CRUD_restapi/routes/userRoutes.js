const express = require('express');

const { body } = require('express-validator/check');

const User = require('../models/userModel');

const userController = require('../controllers/userController');

const router = express.Router();

router.post('/create', [
    body('email').isEmail().withMessage("Provide valid email address")
        .custom((value, { req }) => {
            return User.findOne({ email: value })
                .then(user => {
                    if (user) {
                        return Promise.reject("Email already exists");
                    }
                });
        })
], userController.createUser);

router.get('/read', userController.readUser);

router.get('/read/:userId', userController.readUser);

router.put('/update/:userId', userController.updateUser);

router.delete('/delete/:userId', userController.deleteUser);

module.exports = router;