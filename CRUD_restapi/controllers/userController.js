const User = require('../models/userModel');

const mongoose = require('mongoose');

const { validationResult } = require('express-validator/check');

exports.createUser = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        console.log(errors);
        const error = new Error(errors.errors[0]["msg"]);
        error.statusCode = 422;
        throw error;
    }
    const firstname = req.body.firstname;
    const lastname = req.body.lastname;
    const email = req.body.email;
    const user = new User({
        firstname: firstname,
        lastname: lastname,
        email: email
    });
    user
        .save()
        .then(() => {
            // console.log("User created");
            res.status(201).json({ message: "User created" });
        })
        .catch(err => {
            if (!err.statusCode) {
                err.statusCode = 500;
            }
            next(err);
        })
};

exports.updateUser = (req, res, next) => {
    const userId = req.params.userId;
    const firstname = req.body.firstname;
    const lastname = req.body.lastname;
    const email = req.body.email;
    User.findById(userId)
        .then(user => {
            if (!user) {
                const error = new Error("User not found");
                error.statusCode = 404;
                throw error;
            }
            user.firstname = firstname;
            user.lastname = lastname;
            user.email = email;
            return user.save();
        })
        .then(() => {
            return res.status(200).json({ message: "User updated" });
        })
        .catch(err => {
            if (!err.statusCode) {
                err.statusCode = 500;
            }
            next(err);
        })
};

exports.readUser = (req, res, next) => {
    if (req.params.userId) {
        let userId = req.params.userId;
        User.findById(userId)
            .then(user => {
                if (!user) {
                    const error = new Error("User not found");
                    error.statusCode = 404;
                    throw error;
                }
                return res.status(200).json({ user: user });
            })
            .catch(err => {
                if (!err.statusCode) {
                    err.statusCode = 500;
                }
                next(err);
            })
    }
    else {
        const page = req.query.page;
        let NO_ITEMS_PER_PAGE = 0;
        if (page) {
            NO_ITEMS_PER_PAGE = 2;
        }
        User.find()
            .skip((page - 1) * NO_ITEMS_PER_PAGE)
            .limit(NO_ITEMS_PER_PAGE)
            .then(users => {
                if (!users) {
                    const error = new Error("Users not found");
                    error.statusCode = 404;
                    throw error;
                }
                return res.status(200).json({ users: users });
            })
            .catch(err => {
                if (!err.statusCode) {
                    err.statusCode = 500;
                }
                next(err);
            })
    }
};

exports.deleteUser = (req, res, next) => {
    const userId = req.params.userId;
    User.findByIdAndDelete(userId)
        .then(user => {
            if (!user) {
                const error = new Error("User not found");
                error.statusCode = 404;
                throw error;
            }
            return res.status(200).json({ message: "User deleted", user: user });
        })
        .catch(err => {
            if (!err.statusCode) {
                err.statusCode = 500;
            }
            next(err);
        })
}