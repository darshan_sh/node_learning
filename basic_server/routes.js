const FS = require("fs");

const requestHandler = (req, res) => {
  const url = req.url;
  const method = req.method;
  if (url === "/") {
    res.write(`<html>
    <head><title>My page</title></head>
      <body>
      <form action="/message" method="POST">
      <input type="text" name="message"></input>
      <input type="submit" value="Submit"></input>
      </form>
      </body>
    </html>`
    );
    return res.end();
  }
  if (url === "/message" && method === "POST") {
    const inputRequest = [];
    req.on("data", (chunk) => {
      inputRequest.push(chunk);
    });
    return req.on("end", () => {
      const parsedInput = Buffer.concat(inputRequest).toString();
      const message = parsedInput.split("=")[1];
      FS.writeFile("message.txt", message, () => {
        res.statusCode = 302;
        res.setHeader("Location", "/");
        return res.end();
      });
    });
  }
};

module.exports = requestHandler;
