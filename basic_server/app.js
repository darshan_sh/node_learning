const HTTP = require("http");

const handler = require("./routes");

const server = HTTP.createServer(handler);
server.listen(80);
